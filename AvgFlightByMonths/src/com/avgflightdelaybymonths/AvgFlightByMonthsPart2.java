package com.avgflightdelaybymonths;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class AvgFlightByMonthsPart2 {
	
	public static final String COLUMN_FAMILY = "fData";
	public static final String TABLE_NAME = "FlightData";
	public static final String COLUMN_CANCELLED = "isCancelled";
	public static final String COLUMN_AVG_DELAY = "avgDelay";
	public static final String COLUMN_YEAR = "year";
	public static final byte[] BYTES_COL_FAMILY = Bytes.toBytes(COLUMN_FAMILY);
	public static final byte[] BYTES_COL_CANCELLED = Bytes.toBytes(COLUMN_CANCELLED);
	public static final byte[] BYTES_COL_AVG_DELAY = Bytes.toBytes(COLUMN_AVG_DELAY);
	public static final byte[] BYTES_COL_YEAR = Bytes.toBytes(COLUMN_YEAR);

	public static class FlightMapper extends TableMapper<Text, FloatWritable> {
		
		 private Text rkey = new Text();
		 private FloatWritable rvalue = new FloatWritable();
		 
		  public void map(ImmutableBytesWritable row, Result value, Context context) 
				  throws InterruptedException, IOException {
		      	
		      	String rowkey = new String(value.getRow());
		      	//String[] rowSplit  = rowkey.split("[-]");
		      	//int year = Integer.parseInt(rowSplit[1]);
		      	String val1 = new String(value.getValue(BYTES_COL_FAMILY, BYTES_COL_AVG_DELAY));
		      	//String val2 = new String(value.getValue(BYTES_COL_FAMILY, BYTES_COL_CANCELLED));
		      	//if(year == 2008 && val2.equals("0")){
		      	rkey.set(rowkey);
		      	rvalue.set(Float.parseFloat(val1));
		      	context.write(rkey, rvalue);
		      	//}
		   }
		}
	
	public static class CustomGroupComparator extends WritableComparator{
		
		protected CustomGroupComparator(){
			super(Text.class,true);
		}

		@SuppressWarnings("rawtypes")
		@Override
		public int compare(WritableComparable a, WritableComparable b) {
			
			Text n1 = (Text) a;
			Text n2 = (Text) b;
			String airlineID1 = n1.toString().split("[-]")[0];
			String airlineID2 = n2.toString().split("[-]")[0];
			return airlineID1.compareTo(airlineID2);
		}
	}
	
	public static class CustomPartition extends Partitioner<Text,FloatWritable>{

		@Override
		public int getPartition(Text mykey, FloatWritable delay, int numberOfReducer) {			
			
			String airlineID = mykey.toString().split("[-]")[0];
			return airlineID.hashCode() % numberOfReducer;	
		}
	}
	
	 public static class FlightReducer extends Reducer<Text, FloatWritable, Text, Text>  {

			public void reduce(Text key, Iterable<FloatWritable> value, Context context) 
					throws IOException, InterruptedException {
				
				Map<Integer,Float> delay = new HashMap<Integer,Float>();
				Map<Integer,Integer> count = new HashMap<Integer,Integer>();
				String airlineID = new String();
				for(FloatWritable single : value){
					
					String[] rkeys = key.toString().split("[-]");
					airlineID = rkeys[0];
					int month = Integer.parseInt(rkeys[2]);
					
					if(delay.containsKey(month)){
						delay.put(month, delay.get(month) 
								+ single.get());
						count.put(month, count.get(month) + 1);
					}
					else{
						delay.put(month, single.get());
						count.put(month, 1);
					}
				}
				
				for(Integer single : delay.keySet()){
					delay.put(single, delay.get(single)/count.get(single));
				}
				
				StringBuilder output = new StringBuilder();
				for(Entry<Integer,Float> single : delay.entrySet()){
					output.append("(");
					output.append(single.getKey());
					output.append(",");
					int val =(int) Math.ceil(single.getValue());
					output.append(String.valueOf(val));
					output.append(")");
				}
				
				context.write(new Text(airlineID), new Text(output.toString()));
				
			}
		}
	
	public static void main(String[] args) throws Exception{
		Configuration config = HBaseConfiguration.create();
		
		String[] otherArgs = new GenericOptionsParser(config, args).getRemainingArgs();
	    if (otherArgs.length != 1) {
	      System.err.println("Usage: AvgFlightByMonthsPart2 <out>");
	      System.exit(2);
	    }
		
		Job job = new Job(config, "AvgFlightByMonthsPart2");
		job.setJarByClass(AvgFlightByMonthsPart2.class);     

		FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		SingleColumnValueFilter filter1 = new SingleColumnValueFilter(
				BYTES_COL_FAMILY,BYTES_COL_YEAR,CompareOp.EQUAL,Bytes.toBytes("2008"));
		filterList.addFilter(filter1);
		SingleColumnValueFilter filter2 = new SingleColumnValueFilter(
				BYTES_COL_FAMILY,BYTES_COL_CANCELLED,CompareOp.EQUAL,Bytes.toBytes("0"));
		filterList.addFilter(filter2);
		
		Scan scan = new Scan();
		scan.setCaching(500);        
		scan.setCacheBlocks(false);  
		scan.setFilter(filterList);
		
		TableMapReduceUtil.initTableMapperJob(
		TABLE_NAME, scan,FlightMapper.class, 
		Text.class,FloatWritable.class,job);
		
		job.setReducerClass(FlightReducer.class);    
		job.setGroupingComparatorClass(CustomGroupComparator.class);
		job.setPartitionerClass(CustomPartition.class);
		FileOutputFormat.setOutputPath(job, new Path(args[0]));
		
		System.exit(job.waitForCompletion(true) ? 0 : 1);

	}

}
