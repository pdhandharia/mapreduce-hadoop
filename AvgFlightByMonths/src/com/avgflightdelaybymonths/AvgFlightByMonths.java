package com.avgflightdelaybymonths;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableOutputFormat;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;

import au.com.bytecode.opencsv.CSVParser;

public class AvgFlightByMonths {
	
	public static final String COLUMN_FAMILY = "fData";
	public static final String TABLE_NAME = "FlightData";
	public static final String COLUMN_CANCELLED = "isCancelled";
	public static final String COLUMN_AVG_DELAY = "avgDelay";
	public static final String COLUMN_YEAR = "year";
	public static final byte[] BYTES_COL_FAMILY = Bytes.toBytes(COLUMN_FAMILY);
	public static final byte[] BYTES_COL_CANCELLED = Bytes.toBytes(COLUMN_CANCELLED);
	public static final byte[] BYTES_COL_AVG_DELAY = Bytes.toBytes(COLUMN_AVG_DELAY);
	public static final byte[] BYTES_COL_YEAR = Bytes.toBytes(COLUMN_YEAR);
	
public static class FlightMapper extends Mapper<Object, Text, ImmutableBytesWritable, Writable>{

		@Override
    	protected void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			
			try{
			    
				CSVParser parser = new CSVParser();
				String[] parsedstr = parser.parseLine(value.toString());
	
				String year = parsedstr[0];
				String flightdate = parsedstr[5];
				String airlineID = parsedstr[7];
				String flightNum = parsedstr[10];
				String orig = parsedstr[11];
				Float arrDelayMins;
				if(parsedstr[37].isEmpty()){
					arrDelayMins = 0f;
				}
				else{
					arrDelayMins = Float.parseFloat(parsedstr[37]);
				}
				
				String rowKey = airlineID + "-" + flightdate + "-" + flightNum + "-" + orig;
				
				// check if flight is cancelled or diverted.
				String isCancelled = "0";
				if((int)Float.parseFloat(parsedstr[41]) == 1 || (int)Float.parseFloat(parsedstr[43]) == 1 ){
					isCancelled="1";
				}
				
				Put p = new Put(Bytes.toBytes(rowKey));
				p.add(BYTES_COL_FAMILY, BYTES_COL_CANCELLED, Bytes.toBytes(isCancelled));
				p.add(BYTES_COL_FAMILY, BYTES_COL_AVG_DELAY, Bytes.toBytes(String.valueOf(arrDelayMins)));
				p.add(BYTES_COL_FAMILY, BYTES_COL_YEAR,Bytes.toBytes(year));
				context.write(new ImmutableBytesWritable(Bytes.toBytes(rowKey)), p);

				  //table.put(p);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}

    	
    }


	public static void main(String[] args) throws Exception{
		
		Configuration conf = HBaseConfiguration.create();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: AverageFlightDelay <in> <out>");
	      System.exit(2);
	    }
	    
	    @SuppressWarnings("deprecation")
		HBaseConfiguration hc = new HBaseConfiguration(new Configuration());
	    HTableDescriptor ht = new HTableDescriptor(TABLE_NAME); 
	    ht.addFamily( new HColumnDescriptor(COLUMN_FAMILY));
	    HBaseAdmin hba = new HBaseAdmin( hc );
	    if(hba.tableExists(TABLE_NAME)){
	    	hba.disableTable(TABLE_NAME);
	    	hba.deleteTable(TABLE_NAME);
	    }
	    hba.createTable( ht );
	    hba.close();
	    
	    Job job = new Job(conf, "AvgFlightByMonths");
	    job.setJarByClass(AvgFlightByMonths.class);
	    job.setMapperClass(FlightMapper.class);

	    job.setOutputFormatClass(TableOutputFormat.class);
	    job.getConfiguration().set(TableOutputFormat.OUTPUT_TABLE, TABLE_NAME);
	    job.setOutputKeyClass(ImmutableBytesWritable.class);
	    job.setOutputValueClass(Put.class);
	    job.setNumReduceTasks(0);
	    
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
