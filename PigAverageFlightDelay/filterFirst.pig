REGISTER file:/home/hadoop/lib/pig/piggybank.jar
DEFINE CSVLoader org.apache.pig.piggybank.storage.CSVLoader;

SET default_parallel 10;

-- Step 1
flights1 = LOAD '$INPUT' USING CSVLoader();

-- Step 2
flights2 = LOAD '$INPUT' USING CSVLoader();

-- Step 3
flights1 = FOREACH flights1 GENERATE (chararray)$5 as flightdate, (chararray)$11 as origin, (chararray)$17 as dest, (int) $24 as depTime, (int) $35 as arrTime, (float)$37 as arrDelayMins, (int) $41 as cancelled, (int) $43 as diverted;

flights2 = FOREACH flights2 GENERATE (chararray)$5 as flightdate, (chararray)$11 as origin, (chararray)$17 as dest, (int) $24 as depTime, (int) $35 as arrTime, (float)$37 as arrDelayMins, (int) $41 as cancelled, (int) $43 as diverted;

flights1 = FILTER flights1 BY (cancelled != 1) AND (diverted!=1) AND 
(origin == 'ORD' AND dest != 'JFK') AND ToDate(flightdate,'yyyy-MM-dd') > ToDate('2007-05-31','yyyy-MM-dd') AND ToDate(flightdate,'yyyy-MM-dd') < ToDate('2008-06-01','yyyy-MM-dd');

flights2 = FILTER flights2 BY (cancelled != 1) AND (diverted != 1) AND 
(origin != 'ORD' and dest =='JFK') AND ToDate(flightdate,'yyyy-MM-dd') > ToDate('2007-05-31','yyyy-MM-dd') AND ToDate(flightdate,'yyyy-MM-dd') < ToDate('2008-06-01','yyyy-MM-dd');

-- Step 4
join1 = JOIN flights1 BY (dest,flightdate), flights2 BY (origin,flightdate);

-- Step 5
join1 = FILTER join1 BY $4 < $11;

-- Step 6
result = FOREACH join1 GENERATE (float)($5 + $13) as sumDelay;

groupedResult = group result all;
avg = foreach groupedResult generate AVG(result.sumDelay);

STORE avg INTO '$OUTPUT' USING PigStorage();
