package com.flightDelayByMonths;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import au.com.bytecode.opencsv.CSVParser;

public class AverageFlightDelayByMonths {
	
	public static class FlightKey implements WritableComparable<FlightKey>{

		private Text airlineID;
		private IntWritable month;
		
		public FlightKey(){
			this.airlineID = new Text();
			this.month = new IntWritable();
		}
		
		public FlightKey(Text airlineID, IntWritable month){
			this.airlineID = airlineID;
			this.month = month;
		}
		
		/**
		 * @return the airlineID
		 */
		public Text getAirlineID() {
			return airlineID;
		}

		/**
		 * @param airlineID the airlineID to set
		 */
		public void setAirlineID(Text airlineID) {
			this.airlineID = airlineID;
		}

		/**
		 * @return the month
		 */
		public IntWritable getMonth() {
			return month;
		}

		/**
		 * @param month the month to set
		 */
		public void setMonth(IntWritable month) {
			this.month = month;
		}
		
		@Override
		public void readFields(DataInput in) throws IOException {
			airlineID.readFields(in);
			month.readFields(in);
		}

		@Override
		public void write(DataOutput out) throws IOException {
			airlineID.write(out);
			month.write(out);
			
		}

		@Override
		public int compareTo(FlightKey other) {
			int cmp = airlineID.compareTo(other.airlineID);
			if(cmp == 0){
				cmp = month.compareTo(other.month);
			}
			return cmp;
		}
	}
	
    public static class FlightMapper extends Mapper<Object, Text, FlightKey, FloatWritable>{
    	
    	@Override
    	protected void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			
			try{
			    
				CSVParser parser = new CSVParser();
				String[] parsedstr = parser.parseLine(value.toString());
	
				int year = Integer.parseInt(parsedstr[0]);
				IntWritable month = new IntWritable(Integer.parseInt(parsedstr[2]));				
				Text airlineID = new Text(parsedstr[7]);
				
				// check if flight is cancelled or diverted.
				boolean isCancelled = false;
				if((int)Float.parseFloat(parsedstr[41]) == 1 || (int)Float.parseFloat(parsedstr[43]) == 1 ){
					isCancelled=true;
				}
				
				if(!isCancelled && year == 2008){
					FloatWritable arrDelayMins = new FloatWritable(Float.parseFloat(parsedstr[37]));
					FlightKey mykey = new FlightKey(airlineID, month);
					context.write(mykey, arrDelayMins);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}

    	
    }
    
	public static class CustomGroupComparator extends WritableComparator{
		
		protected CustomGroupComparator(){
			super(FlightKey.class,true);
		}

		@SuppressWarnings("rawtypes")
		@Override
		public int compare(WritableComparable a, WritableComparable b) {
			
			FlightKey m1 = (FlightKey) a;
			FlightKey m2 = (FlightKey) b;
			
			return m1.airlineID.compareTo(m2.airlineID);
		}
	}
	
	public static class CustomPartition extends
					Partitioner<FlightKey,FloatWritable>{

		@Override
		public int getPartition(FlightKey mykey, FloatWritable delay, int numberOfReducer) {			
			return mykey.airlineID.hashCode() % numberOfReducer;	
		}
	}
	
	public static class FlightReducer extends Reducer<FlightKey, FloatWritable, Text,Text>{
		
		protected void reduce(FlightKey key, Iterable<FloatWritable> value, 
				Context context) throws IOException, InterruptedException {
			
			Map<Integer,Float> delay = new HashMap<Integer,Float>();
			Map<Integer,Integer> count = new HashMap<Integer,Integer>();
			
			for(FloatWritable single : value){
				if(delay.containsKey(key.month.get())){
					delay.put(key.month.get(), delay.get(key.month.get()) 
							+ single.get());
					count.put(key.month.get(), count.get(key.month.get()) + 1);
				}
				else{
					delay.put(key.month.get(), single.get());
					count.put(key.month.get(), 1);
				}
			}
			
			for(Integer single : delay.keySet()){
				delay.put(single, delay.get(single)/count.get(single));
			}
			
			StringBuilder output = new StringBuilder();
			for(Entry<Integer,Float> single : delay.entrySet()){
				output.append("(");
				output.append(single.getKey());
				output.append(",");
				int val =(int) Math.ceil(single.getValue());
				output.append(String.valueOf(val));
				output.append(")");
			}
			
			context.write(key.airlineID, new Text(output.toString()));
		}		
	}

	public static void main(String[] args) throws Exception{
		Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: AverageFlightDelay <in> <out>");
	      System.exit(2);
	    }
	    Job job = new Job(conf, "AverageFlightDelayByMonths");
	    job.setJarByClass(AverageFlightDelayByMonths.class);
	    job.setMapperClass(FlightMapper.class);
	    job.setMapOutputKeyClass(FlightKey.class);
	    job.setMapOutputValueClass(FloatWritable.class);
	    job.setGroupingComparatorClass(CustomGroupComparator.class);
	    job.setReducerClass(FlightReducer.class);
	    job.setPartitionerClass(CustomPartition.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);

	}

}
