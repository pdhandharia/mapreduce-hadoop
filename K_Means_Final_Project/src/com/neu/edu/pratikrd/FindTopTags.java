package com.neu.edu.pratikrd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.codehaus.jackson.map.ObjectMapper;

public class FindTopTags {
	
	public static class MyMapper extends Mapper<Object, Text, Text, IntWritable>{
		
	    private final static IntWritable one = new IntWritable(1);
	    private Text word = new Text();
	    ObjectMapper om = new ObjectMapper();
	    
	    public void map(Object key, Text value, Context context
                ) throws IOException, InterruptedException {
	    
	    	Track t = om.readValue(value.toString(),Track.class);
	    	
	    	List<Object> tags = t.getTags();
			for(int i = 0; i < tags.size(); i++){
				@SuppressWarnings("unchecked")
				ArrayList<Object> o = (ArrayList<Object>) tags.get(i);
//				System.out.println(o.get(0) + "  --  " + o.get(1));
				word.set(o.get(0).toString());
				context.write(word, one);
			}
	    }
	}
	
	public static class MyReducer extends Reducer <Text, IntWritable, Text, IntWritable>{
		
		private IntWritable result = new IntWritable();
		
		public void reduce(Text key, Iterable<IntWritable> values, 
				Context context) throws IOException, InterruptedException{
			int sum = 0;
			
			for(IntWritable val : values){
				sum += val.get();
			}
			
			result.set(sum);
			context.write(key, result);
		}
	}

	public static void main(String[] args) throws Exception{
		Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: wordcount <in> <out>");
	      System.exit(2);
	    }
	    Job job = new Job(conf, "Top Tags");
	    job.setJarByClass(FindTopTags.class);
	    job.setMapperClass(MyMapper.class);
	    job.setCombinerClass(MyReducer.class);
	    job.setReducerClass(MyReducer.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(IntWritable.class);
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
