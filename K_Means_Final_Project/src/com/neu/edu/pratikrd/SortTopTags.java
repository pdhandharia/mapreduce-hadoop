package com.neu.edu.pratikrd;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.GenericOptionsParser;


public class SortTopTags {
	
	public static class MyKey implements WritableComparable<MyKey> {
		
		private String word;

		@Override
		public String toString() {
			return word + "-" + String.valueOf(count);
		}

		private Integer count;

		public String getWord() {
			return word;
		}

		public void setWord(String word) {
			this.word = word;
		}

		public Integer getCount() {
			return count;
		}

		public void setCount(Integer count) {
			this.count = count;
		}

		@Override
		public void readFields(DataInput in) throws IOException {
			word = in.readUTF();
			count = in.readInt();
		}

		@Override
		public void write(DataOutput out) throws IOException {
			out.writeUTF(word);
			out.writeInt(count);			
		}

		@Override
		public int compareTo(MyKey arg0) {
			return -this.count.compareTo(arg0.count);
		}
		
	}
	
	public static class MyMapper extends Mapper<Object, Text, MyKey, NullWritable>{
		
		private MyKey mk = new MyKey();
		private NullWritable n = NullWritable.get();
		
		public void map(Object key, Text value, Context context
                ) throws IOException, InterruptedException {
			
			String[] str = value.toString().split("\t");
				mk.setWord(str[0]);
				mk.setCount(Integer.parseInt(str[1].trim()));
				context.write(mk,n);
			}
		}

	
	public static class MyReducer extends Reducer<MyKey, NullWritable, MyKey, NullWritable>{
		
		public void reduce(MyKey mk, Iterable<NullWritable> n, Context context) 
				throws IOException, InterruptedException {
			context.write(mk, NullWritable.get());
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: wordcount <in> <out>");
	      System.exit(2);
	    }
	    Job job = new Job(conf, "Top Tags");
	    job.setJarByClass(SortTopTags.class);
	    job.setMapperClass(MyMapper.class);
	    job.setCombinerClass(MyReducer.class);
	    job.setReducerClass(MyReducer.class);
	    job.setOutputKeyClass(MyKey.class);
	    job.setOutputValueClass(NullWritable.class);
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);

	}

}
