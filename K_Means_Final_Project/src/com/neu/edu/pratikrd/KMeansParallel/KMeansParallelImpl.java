package com.neu.edu.pratikrd.KMeansParallel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import com.neu.edu.pratikrd.util.Helper;

public class KMeansParallelImpl {
	
	public static class KMeansMapper1 extends Mapper<Object, Text, CustomKeyParallel, Text>{
		
		List<Text> centers = new ArrayList<Text>();
		
		@Override
		protected void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			
			Text nearestCenter = null;
			double minDist = Double.MAX_VALUE;
			
			for(Text singleCenter : centers){
				double dist = Helper.getDistance(singleCenter, value);
				if(dist < minDist) {
					minDist = dist;
					nearestCenter = singleCenter;
				}
			}
			
			CustomKeyParallel customkey = new CustomKeyParallel();
			customkey.setCenter(nearestCenter);
			customkey.setMapperNo(new IntWritable(1));
			context.write(customkey, value);
		}

		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			super.setup(context);
			Configuration conf = context.getConfiguration();
			Path center = new Path(conf.get("center.path.1"));
			FileSystem fs = center.getFileSystem(conf);
			
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(center)));
	        String line;
	        line=br.readLine();
	        while (line != null){
	        		Text fileTxt = new Text();
	        		fileTxt.set(line);
	        		centers.add(fileTxt);
	                line=br.readLine();
	        }
	        br.close();
		}
		
	}

	public static class KMeansMapper2 extends Mapper<Object, Text, CustomKeyParallel, Text>{
		
		List<Text> centers = new ArrayList<Text>();
		
		@Override
		protected void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			
			Text nearestCenter = null;
			double minDist = Double.MAX_VALUE;
			
			for(Text singleCenter : centers){
				double dist = Helper.getDistance(singleCenter, value);
				if(dist < minDist) {
					minDist = dist;
					nearestCenter = singleCenter;
				}
			}
			
//			String nearestCenterWithMapperTask = nearestCenter.toString().concat(",2");
//			nearestCenter.set(nearestCenterWithMapperTask);

			CustomKeyParallel customkey = new CustomKeyParallel();
			customkey.setCenter(nearestCenter);
			customkey.setMapperNo(new IntWritable(2));
			context.write(customkey, value);

		}

		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			super.setup(context);
			Configuration conf = context.getConfiguration();
			Path center = new Path(conf.get("center.path.2"));
			FileSystem fs = center.getFileSystem(conf);
			
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(center)));
	        String line;
	        line=br.readLine();
	        while (line != null){
	        		Text fileTxt = new Text();
	        		fileTxt.set(line);
	        		centers.add(fileTxt);
	                line=br.readLine();
	        }
	        br.close();
		}
	}
	
	public static class KMeansMapper3 extends Mapper<Object, Text, CustomKeyParallel, Text>{
		
		List<Text> centers = new ArrayList<Text>();
		
		@Override
		protected void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			
			Text nearestCenter = null;
			double minDist = Double.MAX_VALUE;
			
			for(Text singleCenter : centers){
				double dist = Helper.getDistance(singleCenter, value);
				if(dist < minDist) {
					minDist = dist;
					nearestCenter = singleCenter;
				}
			}
			
//			String nearestCenterWithMapperTask = nearestCenter.toString().concat(",3");
//			nearestCenter.set(nearestCenterWithMapperTask);

			CustomKeyParallel customkey = new CustomKeyParallel();
			customkey.setCenter(nearestCenter);
			customkey.setMapperNo(new IntWritable(3));
			context.write(customkey, value);
		}

		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			super.setup(context);
			Configuration conf = context.getConfiguration();
			Path center = new Path(conf.get("center.path.3"));
			FileSystem fs = center.getFileSystem(conf);
			
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(center)));
	        String line;
	        line=br.readLine();
	        while (line != null){
	        		Text fileTxt = new Text();
	        		fileTxt.set(line);
	        		centers.add(fileTxt);
	                line=br.readLine();
	        }
	        br.close();
		}
	}
	
//public static class KMeansCustomGroupComparator extends WritableComparator{
//		
//		protected KMeansCustomGroupComparator(){
//			super(CustomKeyParallel.class,true);
//		}
//
//		@Override
//		public int compare(WritableComparable a, WritableComparable b) {
//			
//			CustomKeyParallel m1 = (CustomKeyParallel) a;
//			CustomKeyParallel m2 = (CustomKeyParallel) b;
//			
//			return m1.mapperNo.compareTo(m2.mapperNo);
//		}
//	}

	public static class KMeansReducerParallel extends Reducer<CustomKeyParallel, Text, Text, NullWritable>{
		
		public static enum COUNTER{
			CONVERGED
		}

		Vector<Text> centers = new Vector<Text>();
		private MultipleOutputs mos;
		int mapNo;
		
		@Override
		public void setup(Context context) {
				mos = new MultipleOutputs(context);
		}

		@Override
		protected void cleanup(Context context)
				throws IOException, InterruptedException {
			super.cleanup(context);
			mos.close();
			Configuration conf = context.getConfiguration();
			Path center = null;
			FileSystem fs;
			BufferedWriter bw;
			
//			mapNo = Integer.parseInt(conf.get("MAPPERNO"));
			
			if(mapNo == 1){
				center = new Path(conf.get("center.path.1"));
			} else if(mapNo == 2){
				center = new Path(conf.get("center.path.2"));
			} else if(mapNo == 3) {			
				center = new Path(conf.get("center.path.3"));
			}
			if(center != null){
					fs = center.getFileSystem(conf);
					fs.delete(center, true);
					
					bw = new BufferedWriter(new OutputStreamWriter(FileSystem.create(fs, center, FsPermission.getDefault())));
					for(Text singleCenter : centers){
						bw.write(singleCenter.toString());
						bw.write("\n");
					}
					bw.close();
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void reduce(CustomKeyParallel key, Iterable<Text> value, Context context)
				throws IOException, InterruptedException {
			
			int mapperNo = key.getMapperNo().get();
		
			mapNo = mapperNo;
			
			double pop = 0, rock = 0, electronic = 0;
			int count = 0;
			List<Text> valueList = new ArrayList<Text>();
			// calculating new center
			for(Text singleValue : value){
				valueList.add(new Text(singleValue));
				String[] str = singleValue.toString().split(",");
				pop += Double.parseDouble(str[1]);
				rock += Double.parseDouble(str[2]);
				electronic += Double.parseDouble(str[3]);
				count++;
			}
			pop /= count;
			rock /= count;
			electronic /= count;

			// adding new center in the list
			String newC = String.valueOf(pop) + "," + String.valueOf(rock) + "," + String.valueOf(electronic);
			Text newCenter = new Text(newC);
			
			//depending on which mapper task it came from, add new center to 
			// respective center list.
			centers.add(newCenter);
			
//			centers_1.add(newCenter);
			// updating centers in the points
			for(Text singleValue : valueList){
				String[] str = singleValue.toString().split(",");
				str[4] = String.valueOf(pop);
				str[5] = String.valueOf(rock);
				str[6] = String.valueOf(electronic);
				
				String output = str[0] + "," + str[1] + "," + str[2] + "," + str[3] + "," + str[4] + "," + str[5] + "," + str[6];
				Text newPoint = new Text(output);
				
				if(mapperNo == 1){
					mos.write("first", newPoint, NullWritable.get());
				} else if(mapperNo == 2){
					mos.write("second", newPoint, NullWritable.get());				
				} else if(mapperNo == 3){
					mos.write("third", newPoint, NullWritable.get());
				}
//				context.write(newPoint, NullWritable.get());
			}
			
			if (newCenter.compareTo(key.getCenter()) != 0){
	            context.getCounter(COUNTER.CONVERGED).increment(1);
			}
		}
		
	}
	
	public static class CustomKeyParallel implements WritableComparable<CustomKeyParallel>{
		
		private Text center;
		private IntWritable mapperNo;
		
		public CustomKeyParallel(){
			center = new Text();
			mapperNo = new IntWritable(0);
		}

		/**
		 * @return the center
		 */
		public Text getCenter() {
			return center;
		}

		/**
		 * @param center the center to set
		 */
		public void setCenter(Text center) {
			this.center = center;
		}

		/**
		 * @return the mapperNo
		 */
		public IntWritable getMapperNo() {
			return mapperNo;
		}

		/**
		 * @param mapperNo the mapperNo to set
		 */
		public void setMapperNo(IntWritable mapperNo) {
			this.mapperNo = mapperNo;
		}

		@Override
		public void readFields(DataInput in) throws IOException {
			center.readFields(in);
			mapperNo.readFields(in);
		}

		@Override
		public void write(DataOutput out) throws IOException {
			center.write(out);
			mapperNo.write(out);
		}

		@Override
		public int compareTo(CustomKeyParallel arg0) {
			int cmp = this.mapperNo.compareTo(arg0.getMapperNo());
			if(cmp==0){
				cmp = this.center.compareTo(arg0.getCenter());
			}
			return cmp;
		}
	
		@Override
		public String toString() {
			return this.center.toString() + "," + this.mapperNo.toString();
		}

	}
	
	public static class CustomPartitionerParallel extends Partitioner<CustomKeyParallel, Text>{

		@Override
		public int getPartition(CustomKeyParallel key, Text value, int numOfReducer) {
			int mapperNo = key.getMapperNo().get();
			int partitioner = mapperNo % numOfReducer;
			return partitioner;
		}
	}

	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: KMeansParallel <in> <center>");
	      System.exit(2);
	    }
	    
	    int iteration = 1;
	    long counter = 1;
	    
	    do{
	    	conf = new Configuration();
		    Path input_1 = new Path(otherArgs[0] + String.valueOf(iteration - 1) + "/f*");
		    Path input_2 = new Path(otherArgs[0] + String.valueOf(iteration - 1) + "/s*");
		    Path input_3 = new Path(otherArgs[0] + String.valueOf(iteration - 1) + "/t*");
		    
		    Path center_1 = new Path(otherArgs[1] + "center4.txt");
		    Path center_2 = new Path(otherArgs[1] + "center5.txt");
		    Path center_3 = new Path(otherArgs[1] + "center6.txt");

		    Path output = new Path(otherArgs[0] + String.valueOf(iteration));
		    
		    conf.set("center.path.1", center_1.toString());
		    conf.set("center.path.2", center_2.toString());
		    conf.set("center.path.3", center_3.toString());

		    Job job = new Job(conf, "KmeansParallel");
		    job.setJobName("K Means Clustering");
		    
		    FileSystem fs;
		    fs = output.getFileSystem(conf);
		    fs.delete(output,true);
		    
		    job.setJarByClass(KMeansParallelImpl.class);
		    job.setMapperClass(KMeansMapper1.class);
		    job.setMapperClass(KMeansMapper2.class);
		    job.setMapperClass(KMeansMapper3.class);
		    job.setPartitionerClass(CustomPartitionerParallel.class);
		    job.setReducerClass(KMeansReducerParallel.class);
		    job.setMapOutputKeyClass(CustomKeyParallel.class);
		    job.setMapOutputValueClass(Text.class);
//		    job.setGroupingComparatorClass(KMeansCustomGroupComparator.class);
		    job.setOutputKeyClass(Text.class);
		    job.setOutputValueClass(Text.class);
		    
		    MultipleInputs.addInputPath(job, input_1, TextInputFormat.class, KMeansMapper1.class);
		    MultipleInputs.addInputPath(job, input_2, TextInputFormat.class, KMeansMapper2.class);
		    MultipleInputs.addInputPath(job, input_3, TextInputFormat.class, KMeansMapper3.class);
		    
		    FileOutputFormat.setOutputPath(job, output);
		    
		    MultipleOutputs.addNamedOutput(job, "first", TextOutputFormat.class, Text.class, NullWritable.class);
		    MultipleOutputs.addNamedOutput(job, "second", TextOutputFormat.class, Text.class, NullWritable.class);
		    MultipleOutputs.addNamedOutput(job, "third", TextOutputFormat.class, Text.class, NullWritable.class);
		    
		    job.waitForCompletion(true);
		    iteration++;
		    counter = job.getCounters()
                    .findCounter(KMeansReducerParallel.COUNTER.CONVERGED).getValue();
		    
	    }while(counter > 0);
	}

}
