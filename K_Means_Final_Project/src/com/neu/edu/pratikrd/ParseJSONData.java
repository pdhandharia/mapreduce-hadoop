package com.neu.edu.pratikrd;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

public class ParseJSONData {

	public static void main(String[] args) throws Exception {
		String path = "/host/Pratik/Masters/MapReduce/Project DataSet/Last_Fm/lastfm_subset/lastfm_subset/A/A/A/TRAAAAW128F429D538.json";
		
		ObjectMapper om = new ObjectMapper();
		Track t = om.readValue(new File(path),Track.class);
		
		System.out.println("Artist: " + t.getArtist());
		System.out.println("TimeStamp: " + t.getTimestamp());
		System.out.println("Title: " + t.getTitle());
		System.out.println("TrackID: " + t.getTrack_id());
		
		List<Object> similars = t.getSimilars();
		System.out.println("Similar Tracks:");
		for(int i = 0; i < similars.size(); i++){
			@SuppressWarnings("unchecked")
			ArrayList<Object> o = (ArrayList<Object>) similars.get(i);
			System.out.println(o.get(0) + "  --  " + o.get(1));
		}
		
		List<Object> tags = t.getTags();
		System.out.println("Tags:");		
		for(int i = 0; i < tags.size(); i++){
			@SuppressWarnings("unchecked")
			ArrayList<Object> o = (ArrayList<Object>) tags.get(i);
			System.out.println(o.get(0) + "  --  " + o.get(1));
		}
	}

}
