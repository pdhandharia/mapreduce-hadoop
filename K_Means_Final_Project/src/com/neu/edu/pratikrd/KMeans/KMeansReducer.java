package com.neu.edu.pratikrd.KMeans;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class KMeansReducer extends Reducer<Text, Text, Text, NullWritable>{
	
	public static enum COUNTER{
		CONVERGED
	}

	List<Text> centers = new ArrayList<Text>();

	@Override
	protected void cleanup(Context context)
			throws IOException, InterruptedException {
		super.cleanup(context);
		
		Configuration conf = context.getConfiguration();
		Path center = new Path(conf.get("center.path"));
		FileSystem fs = center.getFileSystem(conf);
		fs.delete(center, true);
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fs.create(center,true)));
		for(Text singleCenter : centers){
			bw.write(singleCenter.toString());
			bw.write("\n");
		}
		bw.close();
	}

	@Override
	protected void reduce(Text key, Iterable<Text> value, Context context)
			throws IOException, InterruptedException {
		
		double pop = 0, rock = 0, electronic = 0;
		int count = 0;
		List<Text> valueList = new ArrayList<Text>();
		// calculating new center
		for(Text singleValue : value){
			valueList.add(new Text(singleValue));
			String[] str = singleValue.toString().split(",");
			pop += Double.parseDouble(str[1]);
			rock += Double.parseDouble(str[2]);
			electronic += Double.parseDouble(str[3]);
			count++;
		}
		pop /= count;
		rock /= count;
		electronic /= count;

		// adding new center in the list
		String newC = String.valueOf(pop) + "," + String.valueOf(rock) + "," + String.valueOf(electronic);
		Text newCenter = new Text(newC);
		centers.add(newCenter);
		
		// updating centers in the points
		for(Text singleValue : valueList){
			String[] str = singleValue.toString().split(",");
			str[4] = String.valueOf(pop);
			str[5] = String.valueOf(rock);
			str[6] = String.valueOf(electronic);
			
			String output = str[0] + "," + str[1] + "," + str[2] + "," + str[3] + "," + str[4] + "," + str[5] + "," + str[6];
			Text newPoint = new Text(output);
			
			context.write(newPoint, NullWritable.get());
		}
		
		if (newCenter.compareTo(key) != 0)
            context.getCounter(COUNTER.CONVERGED).increment(1);
		
	}
	
	
	
}
