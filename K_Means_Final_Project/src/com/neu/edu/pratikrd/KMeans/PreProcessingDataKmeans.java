package com.neu.edu.pratikrd.KMeans;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.neu.edu.pratikrd.Track;
import com.neu.edu.pratikrd.util.Helper;

public class PreProcessingDataKmeans {
	
	public static class MyValue implements WritableComparable<MyValue>{
		
		private Text tag;
		private IntWritable tagValue;
		
		public MyValue(){
			tag = new Text();
			tagValue = new IntWritable();
		}

		public Text getTag() {
			return tag;
		}

		public void setTag(Text tag) {
			this.tag = tag;
		}

		public IntWritable getTagValue() {
			return tagValue;
		}

		public void setTagValue(IntWritable tagValue) {
			this.tagValue = tagValue;
		}

		@Override
		public void readFields(DataInput in) throws IOException {
			tag.readFields(in);
			tagValue.readFields(in);			
		}

		@Override
		public void write(DataOutput out) throws IOException {
			tag.write(out);
			tagValue.write(out);
		}

		@Override
		public int compareTo(MyValue o) {
			int cmp = this.tag.compareTo(o.tag);
			if(cmp==0){
				cmp = this.tagValue.compareTo(o.tagValue);
			}
			return cmp;
		}
	}
	
	public static class MyMapper extends Mapper<Object, Text, Text, MyValue>{
		
		private Text word = new Text();
	    private ObjectMapper om = new ObjectMapper();
	    private MyValue myValue = new MyValue();
		
		public void map(Object key, Text value, Context context) 
				throws IOException, InterruptedException,JsonParseException,
				JsonMappingException{
				Track t = om.readValue(value.toString(),Track.class);
				
				List<Object> tags = t.getTags();
				for(int i = 0; i < tags.size(); i++){
					@SuppressWarnings("unchecked")
					ArrayList<Object> o = (ArrayList<Object>) tags.get(i);
//					System.out.println(o.get(0) + "  --  " + o.get(1));
					word.set(t.getArtist().replace(",", ""));
					myValue.setTag(new Text(o.get(0).toString()));
					myValue.setTagValue(new IntWritable(Integer.parseInt(o.get(1).toString())));
					context.write(word, myValue);
				}			
		}
	}
	
	public static class MyReducer extends Reducer<Text, MyValue, Text, NullWritable>{
		
		private Text outputKey = new Text();
		
		public void reduce(Text key, Iterable<MyValue> values,Context context) 
				throws IOException, InterruptedException {
			
			Map<String,Integer> tagsMap = new HashMap<String,Integer>();
			
			for(MyValue singleValue : values){
				String k = singleValue.getTag().toString().toLowerCase();
				//Integer val = singleValue.getTagValue().get();
				Integer val = 1;
				if(tagsMap.containsKey(k)) {
					tagsMap.put(k, val + tagsMap.get(k));
				}
				else { 
					tagsMap.put(k, val); 
				}
			}
			
			String tagValues = Helper.getValuesFromMap(tagsMap);
			
			StringBuilder finalValue = new StringBuilder();
			finalValue.append(key.toString()).append(",").append(tagValues);
			outputKey.set(finalValue.toString());
			context.write(outputKey, NullWritable.get());
		}
	}

	public static void main(String[] args) throws Exception{
		Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: wordcount <in> <out>");
	      System.exit(2);
	    }
	    Job job = new Job(conf, "Top Tags");
	    job.setJarByClass(PreProcessingDataKmeans.class);
	    job.setMapperClass(MyMapper.class);
	    job.setReducerClass(MyReducer.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(MyValue.class);
	    
	    Path out = new Path(otherArgs[1]);
	    FileSystem fs = out.getFileSystem(conf);
	    if(fs.exists(out)){
	    	fs.delete(out, true);
	    }
	    
//	    FileInputFormat.addInputPaths(job, otherArgs[0]);
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);

	}

}
