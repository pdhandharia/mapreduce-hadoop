package com.neu.edu.pratikrd.KMeans;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class KMeansImpl {

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: KMeans <in> <center>");
	      System.exit(2);
	    }
	    
	    int iteration = 1;
	    long counter = 1;
	    
	    do{
	    	conf = new Configuration();
		    Path in = new Path(otherArgs[0] + String.valueOf(iteration - 1) + "/*");
		    Path center = new Path(otherArgs[1]);
		    Path out = new Path(otherArgs[0] + String.valueOf(iteration));
		    conf.set("center.path", center.toString());
		    
		    Job job = new Job(conf, "Kmeans");
		    job.setJobName("K Means Clustering");
		    
		    FileInputFormat.addInputPath(job, in);
		    FileOutputFormat.setOutputPath(job, out);
		    
		    FileSystem fs = out.getFileSystem(conf);
		    fs.delete(out,true);
		    
		    job.setMapperClass(KMeansMapper.class);
		    job.setReducerClass(KMeansReducer.class);
		    job.setJarByClass(KMeansImpl.class);
		    job.setMapOutputKeyClass(Text.class);
		    job.setMapOutputValueClass(Text.class);
		    job.setOutputKeyClass(Text.class);
		    job.setOutputValueClass(Text.class);
		    job.waitForCompletion(true);
		    
		    iteration++;
		    counter = job.getCounters()
                    .findCounter(KMeansReducer.COUNTER.CONVERGED).getValue();
		    
	    }while(counter > 0);

	    
	}

}
