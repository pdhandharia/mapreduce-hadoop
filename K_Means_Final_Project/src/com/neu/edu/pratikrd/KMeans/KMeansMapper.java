package com.neu.edu.pratikrd.KMeans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.neu.edu.pratikrd.util.Helper;

public class KMeansMapper extends Mapper<Object, Text, Text, Text>{
	
	List<Text> centers = new ArrayList<Text>();
	
	@Override
	protected void map(Object key, Text value, Context context)
			throws IOException, InterruptedException {
		
		Text nearestCenter = null;
		double minDist = Double.MAX_VALUE;
		
		for(Text singleCenter : centers){
			double dist = Helper.getDistance(singleCenter, value);
			if(dist < minDist) {
				minDist = dist;
				nearestCenter = singleCenter;
			}
		}
		context.write(nearestCenter, value);
	}

	@Override
	protected void setup(Context context)
			throws IOException, InterruptedException {
		super.setup(context);
		Configuration conf = context.getConfiguration();
		Path center = new Path(conf.get("center.path"));
		FileSystem fs = center.getFileSystem(conf);
		
		BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(center)));
        String line;
        line=br.readLine();
        while (line != null){
        		Text fileTxt = new Text();
        		fileTxt.set(line);
        		centers.add(fileTxt);
                line=br.readLine();
        }
	}
	
	

}
