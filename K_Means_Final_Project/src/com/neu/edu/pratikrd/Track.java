package com.neu.edu.pratikrd;

import java.util.Date;
import java.util.List;

public class Track {
	
	private String artist;
	private String timestamp;
	private List<Object> similars,tags;
	private String track_id;
	private String title;
	
	/**
	 * @return the tags
	 */
	public List<Object> getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(List<Object> tags) {
		this.tags = tags;
	}

	/**
	 * @return the artist
	 */
	public String getArtist() {
		return artist;
	}
	/**
	 * @param artist the artist to set
	 */
	public void setArtist(String artist) {
		this.artist = artist;
	}
	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * @return the similars
	 */
	public List<Object> getSimilars() {
		return similars;
	}
	/**
	 * @param similars the similars to set
	 */
	public void setSimilars(List<Object> similars) {
		this.similars = similars;
	}
	/**
	 * @return the track_id
	 */
	public String getTrack_id() {
		return track_id;
	}
	/**
	 * @param track_id the track_id to set
	 */
	public void setTrack_id(String track_id) {
		this.track_id = track_id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

}
