package com.neu.edu.pratikrd.util;

import java.util.Map;

import org.apache.hadoop.io.Text;

public class Helper {
	
	public static final String pop = "pop";
	public static final String electronic = "electronic";
	public static final String rock = "rock";
	
	public static String getValuesFromMap(Map<String,Integer> tagsMap){
		
		StringBuilder output = new StringBuilder();
		
		if(tagsMap.containsKey(pop)){
			output.append(String.valueOf(tagsMap.get(pop)));
		}
		else {
			output.append("0");
		}
		output.append(",");
		
		if(tagsMap.containsKey(rock)){
			output.append(String.valueOf(tagsMap.get(rock)));
		}
		else {
			output.append("0");
		}
		output.append(",");
		
		if(tagsMap.containsKey(electronic)){
			output.append(String.valueOf(tagsMap.get(electronic)));
		}
		else {
			output.append("0");
		}
		output.append(",0,0,0");
		
		return output.toString();
	}
	
	public static double getDistance(Text center, Text vector){
		double sum = 0;
		String[] c = center.toString().split(",");
		String[] v = vector.toString().split(",");
		
		sum += Math.abs(Double.parseDouble(c[0]) - Double.parseDouble(v[1]));
		sum += Math.abs(Double.parseDouble(c[1]) - Double.parseDouble(v[2]));
		sum += Math.abs(Double.parseDouble(c[2]) - Double.parseDouble(v[3]));
		
		return sum;
	}

}
