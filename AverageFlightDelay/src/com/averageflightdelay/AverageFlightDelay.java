package com.averageflightdelay;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import au.com.bytecode.opencsv.CSVParser;

public class AverageFlightDelay {
	
	// Counter for storing the Average Delay and Total number of flights
	public enum MyCounter{
		AVG_DELAY,
		TOTAL_NO_OF_FLIGHTS
	}
	
	// Serializable class for passing flight details as value from Mapper 
	// to Reducer. This mainly contains arrTime/deptTime, delay time in mins
	// and boolean flag indicating if this flight is leg 1 or leg 2
	public static class FlightDetails 
		implements WritableComparable<FlightDetails> {
		
		private int time;
		private float arrDelayMins;
		private boolean isLeg1;

		public int getTime() {
			return time;
		}

		public void setTime(int time) {
			this.time = time;
		}

		public float getArrDelayMins() {
			return arrDelayMins;
		}

		public void setArrDelayMins(float arrDelayMins) {
			this.arrDelayMins = arrDelayMins;
		}

		public boolean isLeg1() {
			return isLeg1;
		}

		public void setLeg1(boolean isLeg1) {
			this.isLeg1 = isLeg1;
		}

		@Override
		public void readFields(DataInput in) throws IOException {
			time = in.readInt();
			arrDelayMins = in.readFloat();
			isLeg1 = in.readBoolean();
			
		}
		@Override
		public void write(DataOutput out) throws IOException {
			out.writeInt(time);
			out.writeFloat(arrDelayMins);
			out.writeBoolean(isLeg1);
			
		}
		@Override
		public int compareTo(FlightDetails obj) {
			int cmp = time - obj.time;
			if(cmp==0){
				cmp = (int) (arrDelayMins - obj.arrDelayMins);
			}
			return cmp;
		}
	}
	
	// MyMapKey class where I store the intermediate airport and the flight
	// date as key for filtering out records. 
	public static class MyMapKey implements WritableComparable<MyMapKey> {
		
		private Text interim; 
		private Text flightDate;
		
		public MyMapKey(){
			set(new Text(), new Text());
		}
		
		public MyMapKey(Text interim, Text flightDate){
			this.interim = interim;
			this.flightDate = flightDate;
		}
		
		public void set(Text interim, Text flightDate){
			this.interim = interim;
			this.flightDate = flightDate;
		}
		
		public Text getInterim() {
			return interim;
		}

		public void setInterim(Text interim) {
			this.interim = interim;
		}

		public Text getDate() {
			return flightDate;
		}

		public void setDate(Text date) {
			flightDate = date;
		}

		@Override
		public void readFields(DataInput in) throws IOException {
			interim.readFields(in);
			flightDate.readFields(in);
		}

		@Override
		public String toString() {
			return this.interim + ":" + this.flightDate;
		}

		@Override
		public void write(DataOutput out) throws IOException {
			interim.write(out);
			flightDate.write(out);
		}	

		@Override
		public int compareTo(MyMapKey obj) {
			
			int cmp = this.interim.compareTo(obj.interim);
			if(cmp==0){
				cmp = this.flightDate.compareTo(obj.flightDate);
			}			
			return cmp;
		}

		@Override
		public int hashCode() {
			return this.interim.hashCode() * 163 + this.flightDate.hashCode();
		}
	}
	
	// Mapper class that filters flight records that does not satisfy the requirement
	public static class FlightMapper extends 
					Mapper<Object, Text, MyMapKey, FlightDetails>{

		@Override
		protected void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			
			try{
			
				CSVParser parser = new CSVParser();
				
				String[] parsedstr = parser.parseLine(value.toString());
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
				// flight date
				Date flightDate = formatter.parse(parsedstr[5]);
				
				// fetching flight details
				String orig = parsedstr[11];
				String dest = parsedstr[17];
				
				// check if flight is cancelled or diverted.
				boolean isCancelled = false;
				if((int)Float.parseFloat(parsedstr[41]) == 1 || (int)Float.parseFloat(parsedstr[43]) == 1 ){
					isCancelled=true;
				}
				
				// consider only valid flights and emit them. 
				if(isValidFlight(flightDate,orig,dest,isCancelled,formatter)){
					
					float arrDelayMins = Float.parseFloat(parsedstr[37]);
					int arrTime = Integer.parseInt(parsedstr[35]);
					int deptTime = Integer.parseInt(parsedstr[24]);
					
					MyMapKey mykey = new MyMapKey();
					mykey.setDate(new Text(parsedstr[5]));
					
					FlightDetails fDetails = new FlightDetails();
					fDetails.setArrDelayMins(arrDelayMins);

					if(orig.equalsIgnoreCase("ORD")){
						mykey.setInterim(new Text(dest));
						fDetails.setLeg1(true);
						fDetails.setTime(arrTime);
					}
					else{
						mykey.setInterim(new Text(orig));
						fDetails.setLeg1(false);
						fDetails.setTime(deptTime);
					}
					
					context.write(mykey, fDetails);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}

		/*
		 * Validation function to filter out required flights.
		 * */
		private boolean isValidFlight(Date flightDate, String orig,
				String dest, boolean isCancelled, SimpleDateFormat formatter) {
			boolean result = false;
			
			try{
			
				Date startDate = formatter.parse("2007-05-31");
				Date endDate = formatter.parse("2008-06-01");
				result = (isCancelled == false) && flightDate.after(startDate) 
						&& flightDate.before(endDate) 
						&& ( (orig.equalsIgnoreCase("ORD") && !dest.equalsIgnoreCase("JFK"))
						  || (!orig.equalsIgnoreCase("ORD") && dest.equalsIgnoreCase("JFK")) );
				
			}
			catch(Exception e){
				e.printStackTrace();
			}
			return result;
		}	
	}
		
	public static class FlightReducer 
		extends Reducer<MyMapKey,FlightDetails,Text,Text>{

		protected void reduce(MyMapKey mykey, Iterable<FlightDetails> flightList,
				Context context)
				throws IOException, InterruptedException {
			
			List<FlightDetails> chicago = new ArrayList<FlightDetails>();
			List<FlightDetails> jfk = new ArrayList<FlightDetails>();
			
			for(FlightDetails singleFlight : flightList){
				FlightDetails newFlight = new FlightDetails();
				newFlight.setArrDelayMins(singleFlight.getArrDelayMins());
				newFlight.setLeg1(singleFlight.isLeg1());
				newFlight.setTime(singleFlight.getTime());

				if(singleFlight.isLeg1()){
					chicago.add(newFlight);
				}
				else{
					jfk.add(newFlight);
				}
			}
			
			for(FlightDetails singleChicago : chicago){
				for(FlightDetails singlejfk : jfk){

					long result = (long) (singleChicago.arrDelayMins + singlejfk.arrDelayMins);
					
					if(singleChicago.time < singlejfk.time){
						context.getCounter(MyCounter.AVG_DELAY).increment(result);
						context.getCounter(MyCounter.TOTAL_NO_OF_FLIGHTS).increment(1);
					}
				}
			}
			
		}
	}
	
	public static class CustomKeyComparator extends WritableComparator{
		
		protected CustomKeyComparator(){
			super(MyMapKey.class,true);
		}
		
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			
			MyMapKey m1 = (MyMapKey) w1;
			MyMapKey m2 = (MyMapKey) w2;
			
			return m1.compareTo(m2);
		}		
	}
	
	public static class CustomGroupComparator extends WritableComparator{
		
		protected CustomGroupComparator(){
			super(MyMapKey.class,true);
		}

		@Override
		public int compare(WritableComparable a, WritableComparable b) {
			
			MyMapKey m1 = (MyMapKey) a;
			MyMapKey m2 = (MyMapKey) b;
			
			return m1.compareTo(m2);
		}
	}
	
	public static class CustomPartition extends
		Partitioner<MyMapKey,FlightDetails>{

		@Override
		public int getPartition(MyMapKey mykey, FlightDetails myflightDetails, int numberOfReducer) {			
			
			return mykey.getDate().hashCode() % numberOfReducer;
			
		}
		
	}

	public static void main(String[] args) throws Exception{
		
		Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length != 2) {
	      System.err.println("Usage: AverageFlightDelay <in> <out>");
	      System.exit(2);
	    }
	    Job job = new Job(conf, "AverageFlightDelay");
	    job.setJarByClass(AverageFlightDelay.class);
	    job.setMapperClass(FlightMapper.class);
	    job.setMapOutputKeyClass(MyMapKey.class);
	    job.setMapOutputValueClass(FlightDetails.class);
	    job.setGroupingComparatorClass(CustomGroupComparator.class);
	    job.setReducerClass(FlightReducer.class);
	    job.setPartitionerClass(CustomPartition.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	    FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
